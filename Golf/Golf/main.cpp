// Authors:
// Drew Klemola
// Derek Kuykendall
// Jes Robinson
// DSAII Final Project: Minni Golf Sim 2k14 Preemptive Game-of-the-Year Edition
// WASD - move ball
// Closes when hole is reached
// Credit to: OpenGL-Tutorial.org 
// they are bros
// their russian translation was v helpful

#ifndef MAIN_
#define MAIN_

#include <stdio.h>
#include <stdlib.h>

#include <GL/glew.h>

#include <GL/glfw3.h>
GLFWwindow* window;

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;
#include <shader.hpp>
#include <controls.hpp>
#include "Shaping.h"

int main( void )
{
	Shaping shaper = Shaping();

	// Initialise GLFW
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		return -1;
	}

	// Open a window and create its OpenGL context
	window = glfwCreateWindow( 1024, 768, "Playground", NULL, NULL);
	if( window == NULL ){
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// VAO
	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Load Shaders
	GLuint programID = LoadShaders( "SimpleVertexShader.vertexshader", "SimpleFragmentShader.fragmentshader");

	// Get a handle for our "MVP" uniform
	GLuint MatrixID = glGetUniformLocation(programID, "MVP");

	GLint uniformLocation = glGetUniformLocation(programID, "MVP");
	GLuint ModelMatrixID = glGetUniformLocation(programID, "M");

	// Projection matrix : 45� Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	glm::mat4 Projection = glm::perspective(45.0f, 4.0f / 3.0f, 0.1f, 100.0f);

	// Camera matrix
	glm::mat4 View;
	// Model matrix : an identity matrix (model will be at the origin)
	glm::mat4 Model      = glm::mat4(1.0f);
	// Our ModelViewProjection : multiplication of our 3 matrices
	glm::mat4 MVP        = Projection * View * Model; // Remember, matrix multiplication is the other way around
 
	// Make the shapes
	vec3* sphere = shaper.Sphereloop(0.5f);

	// vertex buffer
	static const GLfloat g_vertex_buffer_data[] = {
		// THE PLANE
		10.0f, 0.0f, 10.0f,
		-10.0f, 0.0f, 10.0f,
		-10.0f, 0.0f, -10.0f,
		10.0f, 0.0f, -10.0f,
		//top
		sphere[0][0], sphere[0][1], sphere[0][2],  
		sphere[1][0], sphere[1][1], sphere[1][2],
		sphere[2][0], sphere[2][1], sphere[2][2],
		sphere[0][0], sphere[0][1], sphere[0][2],
		sphere[2][0], sphere[2][1], sphere[2][2],
		sphere[3][0], sphere[3][1], sphere[3][2],
		sphere[0][0], sphere[0][1], sphere[0][2],
		sphere[3][0], sphere[3][1], sphere[3][2],
		sphere[4][0], sphere[4][1], sphere[4][2],
		sphere[0][0], sphere[0][1], sphere[0][2],
		sphere[4][0], sphere[4][1], sphere[4][2],
		sphere[5][0], sphere[5][1], sphere[5][2],
		sphere[0][0], sphere[0][1], sphere[0][2],
		sphere[5][0], sphere[5][1], sphere[5][2],
		sphere[1][0], sphere[1][1], sphere[1][2],

		//middle
		sphere[1][0], sphere[1][1], sphere[1][2],
		sphere[2][0], sphere[2][1], sphere[2][2],
		sphere[6][0], sphere[6][1], sphere[6][2],

		sphere[6][0], sphere[6][1], sphere[6][2],
		sphere[7][0], sphere[7][1], sphere[7][2],
		sphere[2][0], sphere[2][1], sphere[2][2],

		sphere[2][0], sphere[2][1], sphere[2][2],
		sphere[3][0], sphere[3][1], sphere[3][2],
		sphere[7][0], sphere[7][1], sphere[7][2],

		sphere[7][0], sphere[7][1], sphere[7][2],
		sphere[8][0], sphere[8][1], sphere[8][2],
		sphere[3][0], sphere[3][1], sphere[3][2],

		sphere[3][0], sphere[3][1], sphere[3][2],
		sphere[4][0], sphere[4][1], sphere[4][2],
		sphere[8][0], sphere[8][1], sphere[8][2],

		sphere[8][0], sphere[8][1], sphere[8][2],
		sphere[9][0], sphere[9][1], sphere[9][2],
		sphere[4][0], sphere[4][1], sphere[4][2],

		sphere[4][0], sphere[4][1], sphere[4][2],
		sphere[5][0], sphere[5][1], sphere[5][2],
		sphere[9][0], sphere[9][1], sphere[9][2],

		sphere[9][0], sphere[9][1], sphere[9][2],
		sphere[10][0], sphere[10][1], sphere[10][2],
		sphere[5][0], sphere[5][1], sphere[5][2],

		sphere[5][0], sphere[5][1], sphere[5][2],
		sphere[1][0], sphere[1][1], sphere[1][2],
		sphere[10][0], sphere[10][1], sphere[10][2],

		sphere[10][0], sphere[10][1], sphere[10][2],
		sphere[6][0], sphere[6][1], sphere[6][2],
		sphere[1][0], sphere[1][1], sphere[1][2],

		//bottom
		sphere[6][0], sphere[6][1], sphere[6][2],
		sphere[7][0], sphere[7][1], sphere[7][2],
		sphere[11][0], sphere[11][1], sphere[11][2],

		sphere[7][0], sphere[7][1], sphere[7][2],
		sphere[8][0], sphere[8][1], sphere[8][2],
		sphere[11][0], sphere[11][1], sphere[11][2],

		sphere[8][0], sphere[8][1], sphere[8][2],
		sphere[9][0], sphere[9][1], sphere[9][2],
		sphere[11][0], sphere[11][1], sphere[11][2],

		sphere[9][0], sphere[9][1], sphere[9][2],
		sphere[10][0], sphere[10][1], sphere[10][2],
		sphere[11][0], sphere[11][1], sphere[11][2],

		sphere[10][0], sphere[10][1], sphere[10][2],
		sphere[6][0], sphere[6][1], sphere[6][2],
		sphere[11][0], sphere[11][1], sphere[11][2],

		// THE HOLE
		5.0f, 0.1f, 5.0f,
		5.0f, 0.1f, 6.0f,
		5.7f, 0.1f, 5.7f,
		6.0f, 0.1f, 5.0f,
		5.7f, 0.1f, 4.3f,
		5.0f, 0.1f, 4.0f,
		4.3f, 0.1f, 4.3f,
		4.0f, 0.1f, 5.0f,
		4.3f, 0.1f, 5.7f,
		5.0f, 0.1f, 6.0f
	};

	// One color for each vertex.
	static GLfloat g_color_buffer_data[192];	
	int i = 0;
	// plane color: green
	for(i; i<12; i++)
	{
		g_color_buffer_data[i] = (i+2) % 3 == 0 ? 0.7f : 0.1f;
	}
	// first color: cyan
	for(i; i<57; i++)
	{
		g_color_buffer_data[i] = i % 3 == 0 ? 0.0f : 1.0f;
	}
	// second color: yellow
	for(i; i<147; i++)
	{
		g_color_buffer_data[i] = (i+1) % 3 == 0 ? 0.0f : 1.0f;
	}
	// third color: the fuschia
	for(i; i<192; i++)
	{
		g_color_buffer_data[i] = (i+2) % 3 == 0 ? 0.0f : 1.0f;
	}
	// the hole is a void, deVoid of color

	// This will identify our vertex buffer
	GLuint vertexbuffer;
 
	// Generate 1 buffer, put the resulting identifier in vertexbuffer
	glGenBuffers(1, &vertexbuffer);
 
	// The following commands will talk about our 'vertexbuffer' buffer
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	// Give our vertices to OpenGL.
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

	// COLOR BUFFER
	GLuint colorbuffer;
	glGenBuffers(1, &colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_color_buffer_data), g_color_buffer_data, GL_STATIC_DRAW);
			
	do{
		// Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Compute the MVP matrix		
		glm::mat4 ProjectionMatrix = getProjectionMatrix();
		glm::mat4 ViewMatrix = getViewTransformMat();
		glm::mat4 ModelMatrix = glm::mat4(1.0f);
		
		glm::mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

		// Send our transformation to the currently bound shader,
		// in the "MVP" uniform
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
				
		// Use shader
		glUseProgram(programID);GLint uniformLocation = glGetUniformLocation(programID, "MVP");
		
		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
		   0,                  // attribute 0
		   3,                  // size
		   GL_FLOAT,           // type
		   GL_FALSE,           // normalized?
		   0,                  // stride
		   nullptr            // array buffer offset
		);

		// 2nd attribute buffer : colors
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
		glVertexAttribPointer(
			1,                                // attribute
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			nullptr						      // array buffer offset
		);

		// FORCES win condition by closing the program 
		// calculates the view and projection matrices for the plane and the ball
		// ...also returns true if the ball X and Z fall within range of the hole :c
		// the true condition causes the draw loop to break
		// sorry
		if(computeMatricesFromInputs())
		{
			break;
		}

		// draws the ball
		glDrawArrays(GL_TRIANGLE_STRIP, 4,60);

		// Load Second Model Matrix (contains plane and hole
		glm::mat4 ModelMatrix2 = glm::mat4(1.0f);
		ViewMatrix = getViewMatrix();
		// load second MVP Matrix
		glm::mat4 MVP2 = ProjectionMatrix * ViewMatrix * ModelMatrix2;
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP2[0][0]);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
		   0,                  // attribute
		   3,                  // size
		   GL_FLOAT,           // type
		   GL_FALSE,           // normalized?
		   0,                  // stride
		   nullptr            // array buffer offset
		);

		// 2nd attribute buffer : colors
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
		glVertexAttribPointer(
			1,                                // attribute
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			nullptr						      // array buffer offset
		);
		
		// draw the plane
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

		// draw the hole
		glDrawArrays(GL_TRIANGLE_FAN, 64, 10);

		// disables vertex attributes
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
		   glfwWindowShouldClose(window) == 0 );

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}
#endif // MAIN_