#include "Shaping.h"

vec3* Shaping::Sphereloop(float rad)
{
	// create an array of vec3
	vec3* sphere = new vec3[12];
	// angle in degrees
	float theta = 0;

	// set endpoints
	sphere[0] = vec3(0.0f, 0.0f, rad);
	sphere[11] = vec3(0.0f, 0.0f, -rad);

	// create first layer of points
	for(int i = 1; i < 6; i++)
	{
		sphere[i] = vec3(cos(theta)*rad, sin(theta)*rad, 0.5*rad);
		theta += 2*3.14/5;
	}

	// offset angle slightly
	theta = 3.14/5;

	// create second layer of points
	for(int i = 6; i < 11; i++)
	{
		sphere[i] = vec3(cos(theta)*rad, sin(theta)*rad, -0.5*rad);
		theta += 2*3.14/5;
	}

	// returns array
	return sphere;
}