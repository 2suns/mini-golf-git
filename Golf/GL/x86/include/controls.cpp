// Include GLFW
#include <GL/glfw3.h>
extern GLFWwindow* window; // The "extern" keyword here is to access the variable "window" declared in "main"

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/rotate_vector.hpp>
using namespace glm;

#include "controls.hpp"

// View matrices; "ViewMatrix" for ball, "ViewMatrix2" for plane && hole
glm::mat4 ViewMatrix;
glm::mat4 ViewMatrix2;

// other matrices
glm::mat4 ProjectionMatrix;
glm::mat4 ModelMatrix(1.0f);

// getters
glm::mat4 getViewMatrix(){
	return ViewMatrix2;
}

glm::mat4 getViewTransformMat(){
	return ViewMatrix;
}
glm::mat4 getProjectionMatrix(){
	return ProjectionMatrix;
}
glm::mat4 getModelMatrix(){
	return ModelMatrix;
}


// Initial position : on +Z
glm::vec3 position = glm::vec3( -5, 3, 2 ); 

// Vectors of rotation
glm::vec3 xAxis = glm::vec3(1.0f, 0.0f, 0.0f);
glm::vec3 yAxis = glm::vec3(0.0f, 1.0f, 0.0f);
glm::vec3 zAxis = glm::vec3(0.0f, 0.0f, 1.0f);

// Initial Field of View
float FoV = 45.0f;

// Initial Position: ball
float transX = 0.0f, transY = 0.5f, transZ = 0.0f;

// Quaternions
glm::quat Orientation = glm::quat();

// returns True if x and z coordinates lie between 4.5 and 5.5
// ALSO: computes the model, view, and projection matrices of all objects
// despite a number of attempts to correct rolling and axis
// but have chosen to incentivize studying for the final
bool computeMatricesFromInputs(){	

	// Moves ball left and rolls
	if (glfwGetKey( window, GLFW_KEY_A ) == GLFW_PRESS){
		transZ -= 0.001f;
		float scaler = cos(0.001f/2);
		rotate(zAxis, 0.001f, xAxis);
		glm::quat offset = glm::quat(-scaler, sin(0.001f/2) * xAxis.x, sin(0.001f/2) * xAxis.y, sin(0.001f/2) * xAxis.z);
		Orientation = Orientation * offset;
	}
	// Moves ball right and rolls
	if (glfwGetKey( window, GLFW_KEY_D ) == GLFW_PRESS){
		transZ += 0.001f;
		float scaler = cos(0.001f/2);
		rotate(zAxis, -0.001f, xAxis);
		glm::quat offset = glm::quat(scaler, sin(0.001f/2) * xAxis.x, sin(0.001f/2) * xAxis.y, sin(0.001f/2) * xAxis.z);
		Orientation = Orientation * offset;
	}
	// Moves ball forward and rolls
	if (glfwGetKey( window, GLFW_KEY_W ) == GLFW_PRESS){
		transX += 0.001f;
		float scaler = -cos(0.001f/2);
		rotate(xAxis, 0.001f, zAxis);
		glm::quat offset = glm::quat(scaler, sin(0.001f/2) * zAxis.x, sin(0.001f/2) * zAxis.y, sin(0.001f/2) * zAxis.z);
		Orientation = Orientation * offset;
	}
	// Moves ball backward and rolls
	if (glfwGetKey( window, GLFW_KEY_S ) == GLFW_PRESS){
		transX -= 0.001f;
		float scaler = -cos(0.001f/2);
		rotate(xAxis, -0.001f, zAxis);
		glm::quat offset = glm::quat(-scaler, sin(0.001f/2) * zAxis.x, sin(0.001f/2) * zAxis.y, sin(0.001f/2) * zAxis.z);
		Orientation = Orientation * offset;
	}

	// Projection matrix : 45� Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	ProjectionMatrix = glm::perspective(FoV, 4.0f / 3.0f, 0.1f, 100.0f);

	// Camera matrix: "ViewMatrix" for Ball, "ViewMatrix2" for plane & hole
	ViewMatrix       = glm::lookAt(
								position,           // Camera is here
								glm::vec3(2.5f, 0.0f, 2.5f),// and looks here : at the same position, plus "direction"
								glm::vec3(0.0f, 1.0f, 0.0f)                  // Head is up (set to 0,-1,0 to look upside-down)
						   );
		ViewMatrix2       = glm::lookAt(
								position,           // Camera is here
								glm::vec3(2.5f, 0.0f, 2.5f), // and looks here : at the same position, plus "direction"
								glm::vec3(0.0f, 1.0f, 0.0f)                  // Head is up (set to 0,-1,0 to look upside-down)
						   );
		// translate the ball's view matrix
		glm::vec3 translation(transX, transY, transZ);
		ViewMatrix *= glm::translate(glm::mat4(),translation);

		// quaternion rotation
		ViewMatrix *= glm::toMat4(Orientation);
			
	// returns true if the ball is in the hole
	return transX > 4.5 && transX < 5.5 && transZ > 4.5 && transZ < 5.5;
}