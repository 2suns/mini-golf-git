#ifndef CONTROLS_HPP
#define CONTROLS_HPP

bool computeMatricesFromInputs();
glm::mat4 getViewMatrix();
glm::mat4 getViewTransformMat();
glm::mat4 getProjectionMatrix();

#endif