#include "Main.h"
#include "GL\glew.h"
#include "GL\freeglut.h"
#include "GL\freeglut_ext.h"
#include "AngelShaderCompiler.h"

void init( void ) {

    // Create a vertex array object
    GLuint vao;
    glGenVertexArrays( 1, &vao );
    glBindVertexArray( vao );

    // Create and initialize a buffer object
    GLuint buffer;
    glGenBuffers( 1, &buffer );
    glBindBuffer( GL_ARRAY_BUFFER, buffer );

    // Load shaders and use the resulting shader program
    GLuint program = CompileShader( "Shaders//vshader.glsl", "Shaders//fshader.glsl" );
    glUseProgram( program );

    // Initialize the vertex position attribute from the vertex shader
    GLuint loc = glGetAttribLocation( program, "vPosition" );
	// std::cout << loc; // 0 because vPosition assigned via layout in shader
    glEnableVertexAttribArray( loc );
    glVertexAttribPointer( loc, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*) (0) );

    glClearColor( 1.0, 1.0, 1.0, 1.0 ); // white background
}

void display (void)
{
	glClear( GL_COLOR_BUFFER_BIT );     // clear the window
	
	// draw a plane
	/*
	glBegin( GL_QUADS);
	glVertex2f(-.5, -.5);
	glVertex2f(.5, -.5);
	glVertex2f(.5, .5);
	glVertex2f(-.5, .5);
	glEnd();
	*/
	// draw a sphere
	glutSolidSphere(.1, 5, 5);

	glFlush();
}

int main(int argc, char **argv)
{
	glutInit( &argc, argv );
    glutInitDisplayMode( GLUT_RGBA );
    glutInitWindowSize( 512, 512 );
    glutCreateWindow("Minni Golf Sim 2014");
	glewExperimental = GL_TRUE;
    glewInit();
	init();
    glutDisplayFunc( display );
    glutMainLoop();
    return 0;
}